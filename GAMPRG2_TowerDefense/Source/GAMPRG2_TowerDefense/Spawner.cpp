// Fill out your copyright notice in the Description page of Project Settings.


#include "Spawner.h"
#include "EnemyAIController.h"
// Sets default values
ASpawner::ASpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;


}

// Called when the game starts or when spawned
void ASpawner::BeginPlay()
{
	Super::BeginPlay();

	spawnIndex = 0;
	//timer = 3;
	//spawnDelay = 1;
	wave = 1;
	FTimerHandle timerHandle;
	GetWorld()->GetTimerManager().SetTimer(timerHandle, this, &ASpawner::InitiateSpawn, spawnDelay);
	
}

// Called every frame
void ASpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

//Recursive function call when spawning enemy and delay between waves
void ASpawner::InitiateSpawn()
{
	AEnemy* enemy = GetWorld()->SpawnActor<AEnemy>(waveData->enemies[spawnIndex], GetActorLocation(), GetActorRotation());
	
	enemy->waypoints = waypoints;

	if(spawnIndex <= waveData->enemies.Num()-2)
	{
		spawnIndex++;
		FTimerHandle timerHandle;
		GetWorld()->GetTimerManager().SetTimer(timerHandle, this, &ASpawner::InitiateSpawn, spawnDelay);
	}
	else
	{
		wave++;

		if (maxSpawn <= wave)
		{
			spawnIndex = 0;
			FTimerHandle timerHandle;
			GetWorld()->GetTimerManager().SetTimer(timerHandle, this, &ASpawner::InitiateSpawn, timer);
		}
	}
	
}

void ASpawner::SpawnAI()
{
	AEnemy* enemy = GetWorld()->SpawnActor<AEnemy>(waveData->enemies[spawnIndex], GetActorLocation(), GetActorRotation());

	enemy->waypoints = waypoints;

	spawnIndex++;
	FTimerHandle timerHandle;
	GetWorld()->GetTimerManager().SetTimer(timerHandle, this, &ASpawner::SpawnAI, timer);
}

