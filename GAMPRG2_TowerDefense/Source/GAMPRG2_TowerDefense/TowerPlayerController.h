// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TowerPlayerController.generated.h"

class ATowerGhost;
class ATower;
/**
 * 
 */
UCLASS()
class GAMPRG2_TOWERDEFENSE_API ATowerPlayerController : public APlayerController
{
	GENERATED_BODY()
public:
	// Sets default values for this pawn's properties
	ATowerPlayerController();
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	bool isPlacingTower;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	ATowerGhost* towerGhost;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TSubclassOf<ATower> towerToSpawn;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TSubclassOf<ATowerGhost> towerGhostToSpawn;


	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int32 gold;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int32 hp;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupInputComponent() override;

	UFUNCTION()
	void placeTower();

	UFUNCTION(BlueprintImplementableEvent)
		void GameOver();

};
