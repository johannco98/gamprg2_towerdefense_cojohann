// Fill out your copyright notice in the Description page of Project Settings.


#include "Tower.h"
#include "Projectile.h"
#include "Components/SphereComponent.h"
#include "Enemy.h"
#include "Kismet/KismetMathLibrary.h" 	 	
#include "Components/StaticMeshComponent.h"

// Sets default values
ATower::ATower()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;


	turretBody = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("TurretBody"));
	SetRootComponent(turretBody);

	turretGun = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("TurretGun"));
	turretGun->SetupAttachment(turretBody);

	spawnLocation = CreateDefaultSubobject<USceneComponent>(TEXT("SpawnLocation"));
	spawnLocation->SetupAttachment(turretGun);

	attackRangeCollider = CreateDefaultSubobject<USphereComponent>(TEXT("SphereComponent"));
	attackRangeCollider->SetupAttachment(turretBody);
}

// Called when the game starts or when spawned
void ATower::BeginPlay()
{
	Super::BeginPlay();
	
	attackRangeCollider->OnComponentBeginOverlap.AddDynamic(this, &ATower::OnEnterEnemy);
	attackRangeCollider->OnComponentEndOverlap.AddDynamic(this, &ATower::OnExitEnemy);
}

// Called every frame
void ATower::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//Check distance if target is not null
	if (target != NULL)
	{
		fireTimer += DeltaTime;

		if (fireTimer >= fireRate)
		{
			SpawnProjectile();
			fireTimer = 0;
		}

		float dist = FVector::Dist(GetActorLocation(), target->GetActorLocation());

		if (dist > attackRange)
		{

			target = NULL;
		}
	}
	else
	{
		fireTimer = 0;

		//check for null enemies inside array
		for (int32 i = 0; i < enemyInside.Num(); i++)
		{
			if (enemyInside[i] == NULL)
			{
				enemyInside.RemoveAt(i);
				i--;

			}
		}

		/*if (enemyInside[0] == NULL)
		{
			target = NULL;
			enemyInside.RemoveAt(0);
		}*/

		//get lowest distance
		if (target == NULL)
		{
			float lowestDistance = 0;
			AEnemy* nextTarget = NULL;

			for (int32 i = 0; i < enemyInside.Num(); i++)
			{
				if (i == 0)
				{
					nextTarget = enemyInside[i];
					lowestDistance = FVector::Dist(GetActorLocation(), enemyInside[i]->GetActorLocation());

				}
				else
				{

					if (lowestDistance < FVector::Dist(GetActorLocation(), enemyInside[i]->GetActorLocation()))
					{

						nextTarget = enemyInside[i];
						lowestDistance = FVector::Dist(GetActorLocation(), enemyInside[i]->GetActorLocation());
					}
				}
			}

			target = nextTarget;
		}
	}
}

void ATower::SpawnProjectile()
{
	if (target != NULL)
	{
		GEngine->AddOnScreenDebugMessage(-1, 0, FColor::Red, "WHISPER");
		FRotator projectileRotation = UKismetMathLibrary::FindLookAtRotation(spawnLocation->GetComponentLocation(), target->GetActorLocation());
		AProjectile* spawnProjectile = GetWorld()->SpawnActor<AProjectile>(projectile, spawnLocation->GetComponentLocation(), projectileRotation);
	}
}

void ATower::OnEnterEnemy(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	AEnemy* enemy = Cast<AEnemy>(OtherActor);

	if (enemy != NULL && !enemyInside.Contains(enemy))
	{
		enemyInside.Add(enemy);

		if (target == NULL)
			target = enemy;
	}
}

void ATower::OnExitEnemy(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	AEnemy* enemy = Cast<AEnemy>(OtherActor);

	if (enemy != NULL && enemyInside.Contains(enemy))
	{
		enemyInside.Remove(enemy);
		
		if (target == enemy)
			target = NULL;
	}
}

