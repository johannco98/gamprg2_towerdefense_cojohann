// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "Enemy.h"
#include "MyDataAsset.generated.h"
/**
 * 
 */
UCLASS()
class GAMPRG2_TOWERDEFENSE_API UMyDataAsset : public UDataAsset
{
public:
	GENERATED_BODY()
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<TSubclassOf<AEnemy>> enemies;


};
