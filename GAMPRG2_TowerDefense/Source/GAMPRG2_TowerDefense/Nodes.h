// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Nodes.generated.h"

UCLASS()
class GAMPRG2_TOWERDEFENSE_API ANodes : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ANodes();

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	UStaticMeshComponent* staticMesh;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
		USceneComponent* towerLocation;

	bool towerPlaced;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
