// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GAMPRG2_TowerDefenseGameMode.generated.h"

UCLASS(minimalapi)
class AGAMPRG2_TowerDefenseGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AGAMPRG2_TowerDefenseGameMode();
};



