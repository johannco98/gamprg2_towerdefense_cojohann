// Fill out your copyright notice in the Description page of Project Settings.


#include "Enemy.h"
#include "AIController.h"
#include "Components/CapsuleComponent.h"
#include "Kismet/GameplayStatics.h"
#include "PlayerGameMode.h"
#include "TowerPlayerController.h"
#include "Health.h"

// Sets default values
AEnemy::AEnemy()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	healthComp = CreateDefaultSubobject<UHealth>(TEXT("HealthComp"));
}

// Called when the game starts or when spawned
void AEnemy::BeginPlay()
{
	Super::BeginPlay();

	GetCapsuleComponent()->OnComponentHit.AddDynamic(this, &AEnemy::OnCoreCollision);

	waypointIndex = 0;
}

// Called every frame
void AEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	/*AAIController* ai = Cast<AAIController>(this->GetController());

	ai->MoveToActor(waypoints[waypointIndex]);*/

	/*if (FVector::Dist(GetActorLocation(), waypoints[waypointIndex]->GetActorLocation()) < 0.1)
	{
		waypointIndex++;
	}
	if (waypointIndex >= waypoints.Num() - 1)
	{
		Destroy();
	}*/
}

// Called to bind functionality to input
void AEnemy::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AEnemy::OnCoreCollision(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	ATowerPlayerController* playerController = Cast<ATowerPlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0));
	playerController->hp -= 1;

	//Game Over
	if (playerController->hp <= 0)
	{
		playerController->GameOver();
	}
}

