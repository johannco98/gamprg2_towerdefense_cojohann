// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "EnemyAIController.generated.h"

class AEnemy;

UCLASS()
class GAMPRG2_TOWERDEFENSE_API AEnemyAIController : public AAIController
{
	GENERATED_BODY()

public: 
	AEnemyAIController();
	AEnemy* enemy;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	bool isMovingToWaypoint;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void  OnMoveCompleted(FAIRequestID RequestID, EPathFollowingResult::Type Result) override;
	void MoveToWaypoint();
};
