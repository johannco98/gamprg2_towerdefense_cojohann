// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerGameMode.h"

// Sets default values
APlayerGameMode::APlayerGameMode()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void APlayerGameMode::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void APlayerGameMode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

