// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "MyDataAsset.h"
#include "Spawner.generated.h"

class AEnemyAIController;

UCLASS()
class GAMPRG2_TOWERDEFENSE_API ASpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASpawner();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UMyDataAsset* waveData;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<AActor*> waypoints;

	int32 spawnIndex;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int32 wave;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int32 maxSpawn;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float timer;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float spawnDelay;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void InitiateSpawn();
	void SpawnAI();

};
