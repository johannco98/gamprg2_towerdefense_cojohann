// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyAIController.h"
#include "Enemy.h"

AEnemyAIController::AEnemyAIController()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AEnemyAIController::BeginPlay()
{
	Super::BeginPlay();
	enemy = Cast<AEnemy>(GetCharacter());
	isMovingToWaypoint = false;
	//this->MoveToActor(enemy->waypoints[enemy->waypointIndex]);
}

// Called every frame
void AEnemyAIController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (enemy != NULL)
	{
		if (enemy->waypoints.Num() >= 0 && isMovingToWaypoint == false)
		{
			isMovingToWaypoint = true;
			MoveToWaypoint();
		}
	}

	else
	{
		enemy = Cast<AEnemy>(GetCharacter());
		GEngine->AddOnScreenDebugMessage(-1, 0, FColor::Red, "WHISPER");
	}
	
	/*if (FVector::Dist(enemy->GetActorLocation(), enemy->waypoints[enemy->waypointIndex]->GetActorLocation()) < 0.1)
	{
		enemy->waypointIndex++;
		this->MoveToActor(enemy->waypoints[enemy->waypointIndex]);
	}
	if (enemy->waypointIndex >= enemy->waypoints.Num() - 1)
	{
		Destroy();
	}*/
}

void AEnemyAIController::OnMoveCompleted(FAIRequestID RequestID, EPathFollowingResult::Type Result)
{
	enemy->waypointIndex++;
	if (enemy->waypointIndex >= enemy->waypoints.Num())
	{
		Destroy();
	}

	else
	{
		this->MoveToActor(enemy->waypoints[enemy->waypointIndex]);
	}

	//if (enemy->waypointIndex >= enemy->waypoints.Num() - 1)
	//{
	//	Destroy();
	//}
}

void AEnemyAIController::MoveToWaypoint()
{
	this->MoveToActor(enemy->waypoints[enemy->waypointIndex]);
}



