// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "PlayerGameMode.generated.h"


UCLASS()
class GAMPRG2_TOWERDEFENSE_API APlayerGameMode : public AGameModeBase
{
	GENERATED_BODY()
public:
	APlayerGameMode();

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int32 Waves;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
