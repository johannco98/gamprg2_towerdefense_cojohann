// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TowerGhost.generated.h"

class UStaticMeshComponent;

UCLASS()
class GAMPRG2_TOWERDEFENSE_API ATowerGhost : public AActor
{
	GENERATED_BODY()

	
public:	
	// Sets default values for this actor's properties
	ATowerGhost();

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, meta = (AllowPrivateAccess = "true"))
		UStaticMeshComponent* turretBody;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, meta = (AllowPrivateAccess = "true"))
		UStaticMeshComponent* turretGun;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
