// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Tower.generated.h"

class AProjectile;
class USphereComponent;
class AEnemy;
class UStaticMeshComponent;

UCLASS()
class GAMPRG2_TOWERDEFENSE_API ATower : public AActor
{
	GENERATED_BODY()

protected:

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, meta = (AllowPrivateAccess = "true"))
		USphereComponent* attackRangeCollider;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, meta = (AllowPrivateAccess = "true"))
		USceneComponent* spawnLocation;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, meta = (AllowPrivateAccess = "true"))
		UStaticMeshComponent* turretBody;
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, meta = (AllowPrivateAccess = "true"))
		UStaticMeshComponent* turretGun;
	
public:	
	// Sets default values for this actor's properties
	ATower();


	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		TSubclassOf<AProjectile> projectile;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float attackRange;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float fireRate;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		int32 cost;

	AEnemy* target;
	TArray<AEnemy*> enemyInside;

	float fireTimer;

	UFUNCTION()
	void SpawnProjectile();
	UFUNCTION()
	void OnEnterEnemy(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
	void OnExitEnemy(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
