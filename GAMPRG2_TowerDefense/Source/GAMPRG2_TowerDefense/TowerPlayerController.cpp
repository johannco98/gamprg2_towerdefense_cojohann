// Fill out your copyright notice in the Description page of Project Settings.

#include "TowerPlayerController.h"
#include "TowerGhost.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Nodes.h"
#include "Tower.h"

ATowerPlayerController::ATowerPlayerController()
{
	PrimaryActorTick.bCanEverTick = true;
}

void ATowerPlayerController::BeginPlay()
{
	Super::BeginPlay();
	isPlacingTower = false;
}

void ATowerPlayerController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (isPlacingTower == true && towerGhost == NULL)
	{
		towerGhost = GetWorld()->SpawnActor<ATowerGhost>(towerGhostToSpawn);

	}
	else if (isPlacingTower == false && towerGhost != NULL)
	{
		towerGhost->Destroy();
	}
	if (isPlacingTower == true)
	{
		FVector pawnDirection;
		FVector position;
		FVector direction;

		//mouse position to world
		pawnDirection = GetPawn()->GetActorForwardVector();
		DeprojectMousePositionToWorld(position, direction);
		FVector endLocation = position + (direction * 5000);

		//information that we hit object
		FHitResult hit;
		FCollisionQueryParams params;
		TArray<AActor*> ignore;

		//UKismetSystemLibrary::LineTraceSingle(this, position, endLocation, query, false, ignore, EDrawDebugTrace::ForDuration, hit, true);
		//GetWorld()->LineTraceSingleByChannel(hit, position, endLocation, ECC_WorldStatic, params);

		//set ghost tower location to mouse location
		if (GetWorld()->LineTraceSingleByChannel(hit, position, endLocation, ECC_Visibility, params))
		{
			towerGhost->SetActorLocation(hit.Location);
		}
	}
}

void ATowerPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();
	InputComponent->BindAction("Left-click", IE_Pressed, this, &ATowerPlayerController::placeTower);
}

void ATowerPlayerController::placeTower()
{
	if (isPlacingTower)
	{
		FVector pawnDirection;
		FVector position;
		FVector direction;

		//mouse position to world
		pawnDirection = GetPawn()->GetActorForwardVector();
		DeprojectMousePositionToWorld(position, direction);
		FVector endLocation = position + (direction * 5000);

		//information that we hit object
		FHitResult hit;
		FCollisionQueryParams params;
		TArray<AActor*> ignore;

		if (GetWorld()->LineTraceSingleByChannel(hit, position, endLocation, ECC_Visibility, params))
		{
			ANodes* nodes = Cast<ANodes>(hit.Actor);

			if (nodes != NULL)
			{
				if (nodes->towerPlaced == false)
				{
					ATower * tower = GetWorld()->SpawnActor<ATower>(towerToSpawn, nodes->towerLocation->GetComponentLocation(), nodes->GetActorRotation());

					if (tower->cost > gold)
					{
						tower->Destroy();
					}
					else
					{
						nodes->towerPlaced = true;
						gold -= tower->cost; 
					}

				}
			}

		}

		//RESET VALUES
		isPlacingTower = false;
		towerGhost->Destroy();
		towerGhost = NULL;
		towerToSpawn = NULL;
		towerGhostToSpawn = NULL;
	}
}
