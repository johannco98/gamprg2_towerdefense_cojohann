// Copyright Epic Games, Inc. All Rights Reserved.

#include "GAMPRG2_TowerDefenseGameMode.h"
#include "GAMPRG2_TowerDefenseCharacter.h"
#include "UObject/ConstructorHelpers.h"

AGAMPRG2_TowerDefenseGameMode::AGAMPRG2_TowerDefenseGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
